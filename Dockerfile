
FROM docker.io/fedora:34

RUN dnf -y --refresh update \
    && dnf clean all \
    && rm -rf /var/cache/yum

